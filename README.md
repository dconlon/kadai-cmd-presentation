ScalaSyd: Kadai CMDOPTS
=======================

Talk on some type-level programming ideas, through a simple
commandline option parser we use in-house at Atlassian.
